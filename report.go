package main

import (
	"bytes"
	"encoding/json"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

// Report represents a MobSF scan report.
// It only contains the fields we care about.
type Report struct {
	AndroidAPI   *Findings `json:"android_api"`
	IOSAPI       *Findings `json:"ios_api"`
	CodeAnalysis *Findings `json:"code_analysis"`
}

// Findings represents the findings in the MobSF report
func (r *Report) Findings() []*Finding {
	findings := []*Finding{}

	if r.AndroidAPI != nil {
		for _, f := range r.AndroidAPI.Findings {
			findings = append(findings, f)
		}
	}

	if r.IOSAPI != nil {
		for _, f := range r.IOSAPI.Findings {
			findings = append(findings, f)
		}
	}

	if r.CodeAnalysis != nil {
		for _, f := range r.CodeAnalysis.Findings {
			findings = append(findings, f)
		}
	}

	return findings
}

// Findings is a wrapper around []*Finding because this cursed
// report uses an object for what should be an array
type Findings struct {
	Findings []*Finding
}

// Finding represents a single finding
type Finding struct {
	ID          string
	Description string
	Severity    string
	Locations   []*Location
}

// SeverityLevel represents severity level of a finding
func (f *Finding) SeverityLevel() report.SeverityLevel {
	switch f.Severity {
	case "high":
		return report.SeverityLevelHigh
	case "medium":
		return report.SeverityLevelMedium
	case "warning":
		return report.SeverityLevelMedium
	case "low":
		return report.SeverityLevelLow
	case "info":
		return report.SeverityLevelInfo
	case "secure":
		return report.SeverityLevelInfo
	}
	log.Debugf("severity level couldn't be mapped: %s", f.Severity)
	return report.SeverityLevelUnknown
}

type findingData struct {
	Files    map[string]string `json:"files"`
	Metadata *findingMeta      `json:"metadata"`
}

type findingMeta struct {
	ID          string `json:"id"`
	Description string `json:"description"`
	Severity    string `json:"severity"`
}

// UnmarshalJSON decodes a finding
func (f *Findings) UnmarshalJSON(b []byte) error {
	if bytes.Equal(b, []byte("null")) {
		return nil
	}

	findings := map[string]*findingData{}
	if err := json.Unmarshal(b, &findings); err != nil {
		return err
	}

	f.Findings = []*Finding{}

	for key, data := range findings {
		finding := new(Finding)
		id := data.Metadata.ID
		if id == "" {
			id = key
		}
		finding.ID = id
		finding.Description = data.Metadata.Description
		finding.Severity = data.Metadata.Severity
		finding.Locations = []*Location{}
		for file, lines := range data.Files {
			for _, line := range strings.Split(lines, ",") {
				l, err := strconv.Atoi(line)
				if err != nil {
					log.Warnf("failed to parse line number to int. file:%s, line:%s", file, line)
					continue
				}

				loc := &Location{
					FileName:   file,
					LineNumber: l,
				}

				finding.Locations = append(finding.Locations, loc)
			}
		}

		f.Findings = append(f.Findings, finding)
	}

	return nil
}

// Location represents the file name and line number of the finding
type Location struct {
	FileName   string
	LineNumber int
}

// Location converts the finding location to an issue location
func (l *Location) Location() report.Location {
	return report.Location{
		File:      l.FileName,
		LineStart: l.LineNumber,
		LineEnd:   l.LineNumber,
	}
}
