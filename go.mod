module gitlab.com/gitlab-org/security-products/analyzers/mobsf/v2

go 1.15

require (
	github.com/Microsoft/go-winio v0.5.2 // indirect
	github.com/ProtonMail/go-crypto v0.0.0-20220711121315-1fde58898e96 // indirect
	github.com/anmitsu/go-shlex v0.0.0-20200514113438-38f4b401e2be // indirect
	github.com/emirpasic/gods v1.18.1 // indirect
	github.com/imdario/mergo v0.3.13 // indirect
	github.com/kevinburke/ssh_config v1.2.0 // indirect
	github.com/kr/pretty v0.3.0 // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/rogpeppe/go-internal v1.8.1 // indirect
	github.com/sirupsen/logrus v1.9.0
	github.com/stretchr/testify v1.8.1
	github.com/urfave/cli/v2 v2.23.7
	gitlab.com/gitlab-org/security-products/analyzers/command v1.10.1
	gitlab.com/gitlab-org/security-products/analyzers/common/v3 v3.2.2
	gitlab.com/gitlab-org/security-products/analyzers/report/v3 v3.17.0
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.4.0
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d // indirect
	golang.org/x/net v0.0.0-20220708220712-1185a9018129 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
