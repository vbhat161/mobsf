# MobSF analyzer

GitLab Analyzer for iOS and Android projects. It's based on [Mobile Security Framework (MobSF)](https://github.com/MobSF/Mobile-Security-Framework-MobSF).

## How it works

The analyzer uses the [GitLab Analyzers Common Library](https://gitlab.com/gitlab-org/security-products/analyzers/common) as an application framework.

An analysis run has three stages:
1. Match - The analyzer recursively searches the target directory for a "match" which indicates that analysis should be run on the project. The MobSF analyzer will run if the project contains an `.xcodeproj` directory, or an `AndroidManifest.xml` file. It will give up on searching once it reaches `SEARCH_MAX_DEPTH`.
2. Analyze - The analyzer will perform analysis of project source code. The MobSF analyzer does this by zipping the project source code and uploading it to the MobSF service defined by `MOBSF_ADDR`. By default, it expects the MobSF service to be running in a sidecar container at `http://mobsf:8000`. Once analysis is completed, the scan report is passed to the Convert stage.
3. Convert - The MobSF scan report is converted into [GitLab's JSON Report format](https://docs.gitlab.com/ee/user/application_security/sast/#reports-json-format). The resulting artifact is stored in the pipeline artifacts as `gl-sast-report.json`. GitLab is able to display findings on the merge request by parsing this artifact.


## Running it locally

The MobSF analyzer has the MobSF service as a dependency, so two containers need to be run.

1. Start the MobSF service

    ```
    docker run --rm -d \
      -p 8000:8000 \
      -e "MOBSF_API_KEY=key" \
      --name mobsf \
      --net=bridge \
      opensecurity/mobile-security-framework-mobsf:latest
    ```

2. Get the ip of the MobSF container

    ```
    docker inspect -f '{{.NetworkSettings.IPAddress}}' mobsf
    ```

3. Run the analyzer (while within the target project directory)

    ```
    docker run --rm -ti \
      -v "$(pwd):/target" \
      -e ANALYZER_TARGET_DIR=/target/ \
      -e SEARCH_MAX_DEPTH=4 \
      -e MOBSF_ADDR=http://<mobsf_container_ip>:8000/api/v1 \
      -e MOBSF_API_KEY=key \
      mobsf:latest
    ```

## Running in GitLab CI

You can easily add the analyzer as a GitLab CI job by including the SAST template and [enabling experimental features for SAST](https://docs.gitlab.com/ee/user/application_security/sast/#experimental-features).
### Image integration tests

Image integration tests are executed on CI to check the Docker image of the analyzer using [RSpec](https://rspec.info/).
They check the output and exit code of the analyzer, as well as the SAST report it generates.
The image integration tests can also be executed locally, for example, to check an image that was built locally using `docker build`<sup>[3](#unable-to-build-image)</sup>.

#### Running image integration tests using the integration-test Docker image

See the [instructions](https://gitlab.com/gitlab-org/security-products/analyzers/integration-test/-/blob/main/README.md#how-to-run-the-integration-test-docker-container-locally) from the `integration-test` project.

## Special thanks

This feature was a generous contribution by the [H-E-B
Digital](https://digital.heb.com/) team. You can [read more H-E-B's contribution](https://about.gitlab.com/releases/2020/10/22/gitlab-13-5-released/#sast-support-for-ios-and-android-mobile-apps/)
integrating MobSF via the [GitLab Secure Scanning integration
framework](https://docs.gitlab.com/ee/development/integrations/secure.html).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## Troubleshooting

### Unable to build image

If you encounter the error message `Unknown machine architecture: aarch64` while attempting to build an analyzer Docker image locally, this is due to the fact that we currently only support building on an `amd64` architecture, such as an Intel Mac. Other architectures, such as the `ARM` Apple Silicon M1 chip, are not currently supported.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
